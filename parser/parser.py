import sys
import syntax

if len(sys.argv) < 2:
    print("error: too few arguments")
    exit
elif len(sys.argv) > 2:
    print("error: too many arguments")
    exit

syntaxAnalyzer = syntax.Syntax(sys.argv[1])
