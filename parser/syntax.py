import lex

class Syntax:
    def __init__(self,fileName):
        self.lexAnalyzer = lex.Lex(fileName)
        self.nextTok = self.lexAnalyzer.lex()
        self.program()

    '''<program> ::= program <progname> <compound stmt>'''
    def program(self):
        if self.nextTok[1] != 'program':
            print("error: program expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()

        if self.nextTok[1] != 'progname':
            print("error: progname expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()

        self.compoundStmt()

        while True:
            self.nextTok = self.lexAnalyzer.lex()
            if self.nextTok[1] != 'EOF':
                print("error: expected EOF at line " + str(self.nextTok[0]))
            else:
                break

    '''<compound stmt> ::= begin <stmt> {;<stmt>} end'''
    def compoundStmt(self):
        if self.nextTok[1] != 'begin':
            print("error: begin expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()

        while True:
            self.stmt()
            if self.nextTok[1] != 'semicolon':
                break
            else:
                self.nextTok = self.lexAnalyzer.lex()

        self.nextTok = self.lexAnalyzer.lex()
        if self.nextTok[1] != 'end':
            print("error: end expected at line " + str(self.nextTok[0]))

    '''<stmt> ::= <simple stmt> | <structured stmt>'''
    def stmt(self):
        if self.nextTok[1] == 'progname' or self.nextTok[1] == 'variable' or self.nextTok[1] == 'read' or self.nextTok[1] == 'write':
            self.simpleStmt()
        elif self.nextTok[1] == 'begin' or self.nextTok[1] == 'if' or self.nextTok[1] == 'while':
            self.structuredStmt()

    '''<structured stmt> ::= <compound stmt> | <if stmt> | <while stmt>'''
    def structuredStmt(self):
        if self.nextTok[1] == 'begin':
            self.compoundStmt()
        elif self.nextTok[1] == 'if':
            self.ifStmt()
        elif self.nextTok[1] == 'while':
            self.whileStmt()
        else:
            print("error: begin, if, or while expected at line " + str(self.nextTok[0]))

    '''<if stmt> ::= if <expression> then <stmt> | if <expression> then <stmt> else <stmt>'''
    def ifStmt(self):
        self.nextTok = self.lexAnalyzer.lex()
        self.expression()
        self.nextTok = self.lexAnalyzer.lex()
        if self.nextTok[1] != 'then':
            print("error: then expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()
        self.stmt()
        self.nextTok = self.lexAnalyzer.lex()
        if self.nextTok[1] == 'else':
            self.nextTok = self.lexAnalyzer.lex()
            self.stmt()

    '''<while stmt> ::= while <expression> do <stmt>'''
    def whileStmt(self):
        self.nextTok = self.lexAnalyzer.lex()
        self.expression()
        self.nextTok = self.lexAnalyzer.lex()
        if self.nextTok[1] != 'do':
            print("error: do expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()
        self.stmt()

    '''<simple stmt> ::= <assgnment stmt> | <read stmt> | <write stmt> | <comment>'''
    def simpleStmt(self):
        if self.nextTok[1] == 'variable':
            self.assignmentStmt()
        elif self.nextTok[1] == 'read':
            self.readStmt()
        elif self.nextTok[1] == 'write':
            self.writeStmt()
        else:
            print("error: variable, read, or write expected at line " + str(self.nextTok[0]))

    '''<read stmt> ::= read (<variable>{, <variable>})'''
    def readStmt(self):
        while True:
            self.nextTok = self.lexAnalyzer.lex()
            if self.nextTok[1] != 'progname' and self.nextTok[1] != 'variable':
                print("error: variable expected at line " + str(self.nextTok[0]))
            else:
                self.nextTok = self.lexAnalyzer.lex()
            if self.lexTok[1] != 'comma':
                break

    '''<write stmt> ::= write <expression>{, <expression>}'''
    def writeStmt(self):
        while True:
            self.nextTok = self.lexAnalyzer.lex()
            self.expression()
            self.nextTok = self.lexAnalyzer.lex()
            if self.nextTok[1] != 'comma':
                break

    '''<assignment stmt> ::= <variable> := <expression>'''
    def assignmentStmt(self):
        if self.nextTok[1] != 'variable':
            print("error: variable expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()

        if self.nextTok != 'assign':
            print("error: := expected at line " + str(self.nextTok[0]))
        else:
            self.nextTok = self.lexAnalyzer.lex()

        self.expression()

    '''<expression> ::= <simple expr> | <simple expr> <relational operator> <simple expr>'''
    def expression(self):
        while True:
            self.simpleExpr()
            self.nextTok = self.lexAnalyzer.lex()
            if self.relationalOperator() == False:
                break

    '''<relational operator> ::= = | <> | < | <= | >= | >'''
    def relationalOperator(self):
        if self.nextTok[1] == 'equal' or self.nextTok[1] == 'noeq' or self.nextTok[1] == 'less' or self.nextTok[1] == 'leq' or self.nextTok[1] == 'geq' or self.nextTok[1] == 'more':
            return True
        else:
            return False

    '''<simple expr> ::= [<sign>] <term> {<adding operator> <term>}'''
    def simpleExpr(self):
        if self.sign():
            self.nextTok = self.lexAnalyzer.lex()
        else:
            self.term()
        
        self.nextTok = self.lexAnalyzer.lex()
        if self.addingOperator():
            self.nextTok = self.lexAnalyzer.lex()
            self.term()

    '''<sign> ::= + | -'''
    def sign(self):
        if self.nextTok[1] == 'plus' or self.nextTok[1] == 'minus':
            return True
        else:
            return False

    '''<adding operator> ::= + | -'''
    def addingOperator(self):
        return self.sign()

    '''<term> ::= <factor> {<multiplying operator> <factor>}'''
    def term(self):
        self.factor()

        self.nextTok = self.lexAnalyzer.lex()
        if self.multiplyingOperator():
            self.nextTok = self.lexAnalyzer.lex()
            self.factor()

    '''<multiplying operator> ::= *|/'''
    def multiplyingOperator(self):
        if self.nextTok[1] == 'multiply' or self.nextTok[1] == 'divide':
            return True
        else:
            return False

    '''<factor> ::= <variable>|constant|(expression)'''
    def factor(self):
        if self.nextTok[1] != 'progname' and self.nextTok[1] != 'variable' and self.nextTok[1] != 'constant':
            print("error: variable or constant expected at line " + str(self.nextTok[0]))
