import re
from collections import OrderedDict

class Lex:
    '''Some housekeeping before everything gets rolling'''
    def __init__(self, fileName):
        self.fileName = fileName
        # regular expressions for different terminals
        self.expNewLine = re.compile(r"\n") # we handle newline in a special way
        self.expSemicolon = re.compile(r";")
        self.expAssign = re.compile(r":=")
        self.expLeftParen = re.compile(r"\(")
        self.expRightParen = re.compile(r"\)")
        self.expComma = re.compile(r"\,")
        self.expPlus = re.compile(r"\+")
        self.expMinus = re.compile(r"-")
        self.expAstrick = re.compile(r"\*")
        self.expSlash = re.compile(r"/")
        self.expEqual = re.compile(r"=")
        self.expLess = re.compile(r"<")
        self.expMore = re.compile(r">")
        self.expNoeq = re.compile(r"<>")
        self.expLeq = re.compile(r"<=")
        self.expGeq = re.compile(r"=>")
        self.expComment = re.compile(r"#.+")
        self.expProgram = re.compile(r"\bprogram\b")
        self.expProgname = re.compile(r"\b[A-Z][A-Za-z0-9]*\b")
        self.expBegin = re.compile(r"\bbegin\b")
        self.expEnd = re.compile(r"\bend\b")
        self.expRead = re.compile(r"\bread\b")
        self.expWrite = re.compile(r"\bwrite\b")
        self.expIf = re.compile(r"if\b")
        self.expThen = re.compile(r"then\b")
        self.expElse = re.compile(r"else\b")
        self.expWhile = re.compile(r"while\b")
        self.expDo = re.compile(r"do\b")
        self.expVariable = re.compile(r"[A-Za-z][A-Za-z0-9]*\b")
        self.expConstant = re.compile(r"[0-9]+\b")
        # dictionary of the regular expressions with the token name
        tmp = (
                ('semicolon', self.expSemicolon), 
                ('assign', self.expAssign),
                ('leftparen', self.expLeftParen),
                ('rightparen', self.expRightParen),
                ('comma', self.expComma),
                ('plus', self.expPlus),
                ('minus', self.expMinus),
                ('multiply', self.expAstrick),
                ('divide', self.expSlash),
                ('equal', self.expEqual),
                ('less', self.expLess),
                ('more', self.expMore),
                ('noeq', self.expNoeq),
                ('leq', self.expLeq),
                ('geq', self.expGeq),
                ('comment', self.expComment),
                ('program', self.expProgram),
                ('progname', self.expProgname),
                ('begin', self.expBegin),
                ('end', self.expEnd),
                ('read', self.expRead),
                ('write', self.expWrite),
                ('if', self.expIf),
                ('then', self.expThen),
                ('else', self.expElse),
                ('while', self.expWhile),
                ('do', self.expDo),
                ('var', self.expVariable),
                ('const', self.expConstant)
            )
        self.tokens = OrderedDict(tmp)
        # line counter for lex
        self.lineCount = 1
        # read the file
        self.readSource()

    '''Read the source file and get it ready for lex()'''
    def readSource(self):
        self.sourceFile = open(self.fileName, "r")
        if self.sourceFile.mode == "r":
            self.sourceCode = self.sourceFile.read() # lex() should use self.sourceCode
            self.sourceFile.close()
        else:
            print("Error: cannot open file " + fileName)
            exit

    '''The lex analyzer itself, using regluar expressions'''
    def lex(self):
        while len(self.sourceCode) > 0:
            for key, exp in self.tokens.items():
                match = exp.match(self.sourceCode)
                if match:
                    self.sourceCode = self.sourceCode[match.end():]
                    if self.expNewLine.match(self.sourceCode): # yep, newline is treated differently than everything else
                        self.lineCount = self.lineCount + 1
                    self.sourceCode = self.sourceCode.lstrip()
                    if key == 'comment':
                        continue
                    else:
                        return (self.lineCount, key, match)
            else:
                print("lex(): syntax error: illegal token at line " + str(self.lineCount))
                self.sourceCode = self.sourceCode[1:]
                if self.expNewLine.match(self.sourceCode):
                    self.lineCount = self.lineCount + 1
                self.sourceCode.lstrip()
        else:
            return (self.lineCount, 'EOF', 'EOF')
